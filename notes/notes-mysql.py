from flask import Flask, jsonify
import json
from werkzeug.exceptions import NotFound
import mysql.connector
from mysql.connector import Error

app = Flask(__name__)



@app.route("/", methods=['GET'])
def hello():
    return jsonify({
        "uri": "/",
        "subresource_uris": {
            "notes": "/notes"
        }
    })


@app.route("/notes", methods=['GET'])
def notes():

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    c.execute('SELECT * FROM notes')
    all_rows = c.fetchall()

    conn.close()

    resp = [
        {'id': row[0], 'text': row[1], 'date': row[2]}
        for row in all_rows
    ]

    return jsonify(resp)

if __name__ == "__main__":
    app.run(host= "0.0.0.0", port=8083, debug=True)
