from flask import Flask, jsonify
from werkzeug.exceptions import NotFound, ServiceUnavailable
import json
import requests


app = Flask(__name__)


@app.route("/", methods=['GET'])
def hello():
    return jsonify({
        "uri": "/",
        "subresource_uris": {
            "add": "/add/<x>/<y>",
            "pow": "/pow/<x>/<y>",
            "notes": "/notes"
        }
    })


@app.route("/add/<int:x>/<int:y>", methods=['GET'])
def add(x ,y):
    try:
        return jsonify(requests.get("http://0.0.0.0:8081/add/{}/{}".format(x, y)).json())
    except requests.exceptions.ConnectionError:
        raise ServiceUnavailable("The Addition service is unavailable.")


@app.route("/pow/<int:x>/<int:y>", methods=['GET'])
def pow(x, y):
    try:
        return jsonify(requests.get("http://0.0.0.0:8082/pow/{}/{}".format(x, y)).json())
    except requests.exceptions.ConnectionError:
        raise ServiceUnavailable("The Power service is unavailable.")

@app.route("/notes", methods=['GET'])
def notes():
    try:
        return jsonify(requests.get("http://0.0.0.0:8083/notes").json())
    except requests.exceptions.ConnectionError:
        raise ServiceUnavailable("The Notes service is unavailable.")



if __name__ == "__main__":
    app.run(host= '0.0.0.0', port=8080, debug=True)