#!/bin/bash -e

#build images of each microservices
docker build -t gcr.io/project4-233216/gateway:v1 ./gateway
docker build -t gcr.io/project4-233216/add:v1 ./add
docker build -t gcr.io/project4-233216/pow:v1 ./pow
docker build -t gcr.io/project4-233216/notes:v1 ./notes

#Configure Docker command-line tool to authenticate to Container Registry
gcloud auth configure-docker

#push the images in your cloud container registery
docker push gcr.io/project4-233216/gateway:v1
docker push gcr.io/project4-233216/add:v1
docker push gcr.io/project4-233216/pow:v1
docker push gcr.io/project4-233216/notes:v1

