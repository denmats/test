#!/bin/bash

#Change [YOUR MYSQL POD]. name of your mysql pod you can take from: kubectl get pods --namespace=dev1
kubectl exec -it mysql-5bfd5f74dd-pgwd6  --namespace=dev1 -- mysql -uroot -p123<<EOF
/*create database mydb*/
create database if not exists mydb;

/*turn into mydb*/
use mydb;

/*create tables: users, posts, comment*/
create table if not exists notes(id int auto_increment primary key, notes varchar(255), date date);

/*fill table notes some data*/
insert into notes values(null, 'post1', '2016-02-01'),(null, 'post2', now()),(null, 'post8', '2018-03-25');

EOF

echo -e  "The database 'mydb' is created, table 'notes' is created, some records are in the table 'notes'"
