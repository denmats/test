# IT Academy final test

#Before starting you need:
# - create service account with roles: Storage Admin, Cloud SQL Admin, Cloud SQL viewer, Kubernetes Engine Admin, Editor
# - create a cluster on Kubernetes (give all cloud permission)

#To build images of each microservices and push them to your Cloud Container Registery run the command:
#   perl build-push-images.sh   #if you will asked Do you want to continue, hit Y


#Create persistent disk(for mysql db), mysql deploy, myslq secret, mysql service,
#Also create and deploy all microservices services on Kubernetes
#   perl create.sh 

#Before create database in your MySQl you need:
# run the command: kubectl get pods --namespace=dev1
# ! - from output copy 'mysql-.....' and paste in the script insted of [YOUR MYSQL POD]


#Create database 'db', create table 'notes', put some data in table 'notes'
# perl db-mysql-fill-data.sh

#After completing script you will get communicate: "The database is created ,..."
