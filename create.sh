#!/bin/bash -e

#Create namespace dev1
kubectl create namespace dev1

#mysql persistent disk, secret, deployment, service
kubectl apply -f mysql-volumeclaim.yaml --namespace=dev1

kubectl create secret generic mysql --from-literal=password=123 --namespace=dev1

kubectl apply -f mysql.yaml --namespace=dev1
kubectl apply -f mysql-service.yaml --namespace=dev1


#deployment of microservices
kubectl create -f gateway.yaml --namespace=dev1
kubectl create -f add.yaml --namespace=dev1
kubectl create -f pow.yaml --namespace=dev1
kubectl create -f notes.yaml --namespace=dev1

#create services of microservices
kubectl create -f deployment-svc.yaml --namespace=dev1
